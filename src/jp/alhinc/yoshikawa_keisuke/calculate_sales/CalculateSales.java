package jp.alhinc.yoshikawa_keisuke.calculate_sales;

//インポート
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

//売上集計課題
public class CalculateSales
{
	public static void main(String[] args)
	{
		//バッファを介してデータ読み込み
		BufferedReader store = null; //支店定義ファイル
		BufferedReader sales = null; //売上ファイル

		//Mapの宣言
		Map<String,String> storeBox = new HashMap<>(); //支店コード、支店名
		Map<String,Long> salesBox = new HashMap<>(); //支店コード、売上金

		//コマンドライン引数で指定されたディレクトリからファイル名が数字８桁且つ、拡張子rcdのファイルを検索
		FilenameFilter filter = new FilenameFilter() //ファイルフィルタ作成
		{
			public boolean accept(File file, String str)
			{
				return str.matches("^[0-9]{8}.rcd$"); //数字８桁且つ、拡張子rcdのファイル
			}
		};

		try //例外の発生を調べる
		{
			File storeFiles = new File(args[0], "branch.lst"); //ファイル呼び出し（支店定義ファイル）
			store = new BufferedReader(new FileReader(storeFiles)); //storeにfileを代入（支店定義ファイル）

			if(!storeFiles.exists()) //if文 ファイルが存在しない場合
			{
	            System.out.println("支店定義ファイルが存在しません"); //コンソール出力
	            return;
	        }

			String line; //String型変数の宣言

            while((line = store.readLine()) != null) //while文 支店定義ファイルを1行読み込み、空白でない限り繰り返す
			{
            	String[] str = line.split(","); //Split文 支店コードと支店名の分割
            	storeBox.put(str[0], str[1]); //支店コードと支店名を分割してstoreBoxに入れ込む

            	if(!str[0].matches("^[0-9]{3}$") || str.length != 2) //if文 ファイルフォーマットが不正の場合
    			{
                    System.out.println("支店定義ファイルのフォーマットが不正です"); //コンソール出力
                    return;
                }
			}

            File[] salesFiles = new File(args[0]).listFiles(filter); //ファイル呼び出し（売上ファイル）

            for(int i = 0; i < salesFiles.length; i++) //for文 Mapのループ
            {
            	int fileNumber = Integer.parseInt(salesFiles[i].getName().substring(0, 8)); //ファイル名の数字抽出

				if(fileNumber - i != 1) //if文 売上ファイルが連番になっていない場合
				{
					System.out.println("売上ファイル名が連番になっていません"); //コンソール出力
					return;
				}

            	sales = new BufferedReader(new FileReader(salesFiles[i])); //salesに売上ファイルを代入
            	String code = sales.readLine(); //売上ファイルを１行読み込む（支店コード）
            	String money = sales.readLine(); //売上ファイルを１行読み込む（売上金）
            	Long amount = Long.parseLong(money); //String型からLong型に変換

            	if(!storeBox.containsKey(code)) //if文 キーが存在するか確認
            	{
            		System.out.println(salesFiles[i].getName() + "の支店コードが不正です"); //コンソール出力
            		return;
            	}

            	if(sales.readLine() != null) //if文 売上ファイル３行目が空白でない場合
            	{
            		System.out.println(salesFiles[i].getName() + "のフォーマットが不正です"); //コンソール出力
            		return;
            	}

            	if(salesBox.containsKey(code)) //キーが存在するか確認
            	{
            		Long amount2 = salesBox.get(code); //支店の値を取り出す
            		Long amount3 = amount2 + amount; //同じ支店コードの値を足す
            		salesBox.put(code, amount3); //salesBoxに支店コード、売上金を入れ込む

            		if(amount3.toString().length() > 10) //if文 合計金額が１０桁を超えた場合
    				{
    					System.out.println("合計金額が10桁を超えました"); //コンソール出力
    					return;
    				}
            	}

            	else
            	{
            		salesBox.put(code, amount); //salesBoxに支店コード、売上金を入れ込む
            	}
            }

            File totalSalesFiles = new File(args[0], "branch.out"); //ファイル呼び出し（支店別集計ファイル）
            FileWriter fw = new FileWriter(totalSalesFiles); //ファイルに書き出す
            BufferedWriter bw = new BufferedWriter(fw); //バッファを介して、ファイルに書き出す
            PrintWriter pw = new PrintWriter(bw); //文字型出力ストリーム作成

            for(Map.Entry<String,String> entry : storeBox.entrySet()) //for文 Mapのループ
			{
				pw.println(entry.getKey() + "," + entry.getValue() + "," + salesBox.get(entry.getKey()));
			}

			pw.close(); //pwを閉じる
        }

		catch(IOException e) //例外が起きた場合に、次のコードを出力する
		{
			System.out.println("予期せぬエラーが発生しました"); //コンソール出力
		}

		finally //例外の発生に関わらず、最後に行う処理
		{
			if(store != null)
			{
				try
				{
					store.close(); //storeを閉じる
				}

				catch(IOException e) //例外が起きた場合に、次のコードを出力する
				{
					System.out.println("closeできませんでした"); //コンソール出力
				}
			}

			if(sales != null)
			{
				try //例外の発生を調べる
				{
					sales.close(); //salesを閉じる
				}

				catch(IOException e) //例外が起きた場合に、次のコードを出力する
				{
					System.out.println("closeできませんでした"); //コンソール出力
				}
			}
		}
    }
}